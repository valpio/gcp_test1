package com.aquant.etl;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;

public class Driver{
    //  This app is used for extracting shortman's short ratio info
    public static void main(String[] args) throws Exception {
        HashMap<String,String> config = getConfig(args[0]);
        final String db_url 	= config.get("db_url");
        final String db_uname	= config.get("db_uname");
        final String db_pword	= config.get("db_pword");
        final String db_name	= config.get("db_name");
        final String tbl_name 	= config.get("tbl_name");
        final String dl_link	= config.get("dl_link");
        final String sv_link    = config.get("sv_link");
        final String fl_location= config.get("fl_location");
        final String fl_delimiter  = config.get("fl_delimiter");
        final char delimiter = fl_delimiter.charAt(0);

        getCer(fl_location);
        int current_row_count = 0;
        final URL url = new URL(null,dl_link,new sun.net.www.protocol.https.Handler());
        HttpsURLConnection http_con = (HttpsURLConnection)url.openConnection();
        http_con.setDoOutput(true);
        http_con.connect();
        http_con.getServerCertificates();
        BufferedReader br = new BufferedReader(new InputStreamReader(http_con.getInputStream()));


        FileUtils.copyURLToFile(
                new URL(dl_link),
                new File("./text.csv"),
                10000,
                10000);


        final CSVParser parser = new CSVParser(br, CSVFormat.EXCEL.withDelimiter(delimiter).withFirstRecordAsHeader());

        System.out.println(parser);


//        for (final CSVRecord record : parser) {
//            String query = query_prefix;
//
//            for ( int i =0; i < record.size() ;  i++){
//                query = query + record.get(i).replaceAll("'", "''") + query_midfix;
//            }
//            query = query.substring(0, query.length()-3)+ query_postfix;
//            stmt.execute(query);
//        }

    }

    private static void getCer (String fl_location) throws Exception {
        KeyStore trustStore  = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null);
        File initialFile = new File(fl_location);
        InputStream targetStream = new FileInputStream(initialFile);
        BufferedInputStream bis = new BufferedInputStream(targetStream);
        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        while (bis.available() > 0) {
            Certificate cert = cf.generateCertificate(bis);
            trustStore.setCertificateEntry("fiddler"+bis.available(), cert);
        }
    }

    private static HashMap<String, String> getConfig(String input) throws IOException {
        String line = null;
        FileReader fileReader =  new FileReader(input);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        HashMap<String, String> ans = new HashMap<String,String>();
        while((line = bufferedReader.readLine()) != null)
            ans.put(line.substring(0, line.indexOf("=")), line.substring(line.indexOf("=")+1,line.length()));

        bufferedReader.close();
        fileReader.close();
        return ans;
    }
}